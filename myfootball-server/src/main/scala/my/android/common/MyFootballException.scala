package my.android.common

class MyFootballException(val cause: Throwable, val formatMsg: String, val args: Any*) extends Exception(formatMsg.format(args: _*), cause) {
  val msg = formatMsg.format(args: _*)

  def this(formatMsg: String, args: Any*) = this(null: Throwable, formatMsg, args)
}
