package my.android.model

import akka.actor.Actor
import collection.mutable.HashMap
import com.javadocmd.simplelatlng.{LatLngTool, LatLng}
import com.javadocmd.simplelatlng.util.LengthUnit
import com.mongodb.{DBObject, BasicDBList, BasicDBObject, Mongo}

import akka.config.Supervision.Permanent
import com.mongodb.casbah.Imports._
import com.novus.salat.global._
import com.novus.salat._
import my.android.server.services.{UserGeoContext, GeoLocation, GeoPoint}
import my.android.server.resources.Cinema

case class RegisterUser(name: String, login: String, password: String, callback: PartialFunction[Option[User], Unit])
case class GetUser(id: String, accountType:String, callback: PartialFunction[Option[User], Unit])
case class GetVKUser(id: String, token:String, callback: PartialFunction[Option[User], Unit])

case class GetUserReply(id: String)

case class GetGeoDistance(callback : PartialFunction[Option[Double], Unit])
case class GetClosest(to: GeoPoint ,callback : PartialFunction[Seq[GeoPoint], Unit])
case class GetUserGeoContext(to: GeoPoint ,callback : PartialFunction[UserGeoContext, Unit])

case class GetAllCinemas(callback : PartialFunction[Seq[GeoPoint], Unit])

class InMemoryModelActor extends Actor {
  self.lifeCycle = Permanent
  self.id = "model"

  lazy val mongoActor = Actor.registry.actorFor[MongoActor].head

  private val usersPerId = new HashMap[String, User]


  override def preStart() {
    usersPerId.put("deil87", new User("deil87", "Deil","123"))
    super.preStart()
  }

  protected def receive = {
    case RegisterUser(name, login, pwd, callback) =>
      callback(addUserToDB(new User(login, name, pwd)))
    case GetUser(id, accountToken, callback) =>
      callback({
        usersPerId.get(id)
          .orElse(mongoActor.ask(GetUserMongo(id, accountToken)).get.asInstanceOf[Option[User]])
            .orElse(mongoActor.ask(CreateUserMongo(id, accountToken)).get.asInstanceOf[Option[User]])
      })
    case GetUserReply(id) =>
      self reply usersPerId.get(id)
    case GetGeoDistance(callback) =>
      callback(Option(getDistance))
    case GetClosest(point, callback) =>
      callback(getClosest(point))
    case GetUserGeoContext(point, callback) =>
      callback(getUserGeoContext(point))
    case GetAllCinemas(callback) =>
      callback(getAllCinemas)
  }

  def addUserToDB(user: User): Option[User] = {
    mongoActor ! AddUserByRegistration(user)
    Option(user)
  }

  def getUserGeoContext(p: GeoPoint): UserGeoContext = {
    val result = mongoActor.ask(ConstructUserGeoContext(p), 60000L)
    val r = result.get.asInstanceOf[UserGeoContext]
    r
  }

  def getClosest(p: GeoPoint): Seq[GeoPoint] = {
    val result = mongoActor.ask(GetClosestWithinCircle(p, 0.0001), 60000L) onTimeout { _ =>
      println("Timed out!")
    }
    val r = result.get.asInstanceOf[Seq[DBObject]].map(_.get("loc").asInstanceOf[BasicDBList]).map(p => GeoPoint(p.get(0).asInstanceOf[Double],p.get(1).asInstanceOf[Double]))
    r
  }

  def getAllCinemas: Seq[GeoPoint] = {
    val result = mongoActor.ask(GetAllMoscowCinemas(), 60000L)
    result.get.asInstanceOf[Seq[Cinema]].map(_.center)
  }

  def getDistance: Double = {
    val mongo = new Mongo( "localhost" , 27017 )
    val db = mongo.getDB( "test" )
    val coll = db.getCollection("foo")
    val doc = new BasicDBObject()
    doc.put("routeId", "9327014.tcx")
    doc.put("login", "1105409")
    val loc = new BasicDBObject()
    loc.put("x", 5.1400218)
    loc.put("y", 52.0614276)
    doc.put("loc", loc)
    coll.insert(doc)

    val query = new BasicDBObject()
    val location = new BasicDBObject()
    val near = new BasicDBList()
    near.put( "0", 5.15 )
    near.put( "1", 52.069 )
    location.put("near", near)
    query.put("loc",loc)
    /* coll is a DBCollection */
    coll.find(query).limit(50)

    val point1 = new LatLng("55.75879".toDouble, "37.40633".toDouble)
    val point2 = new LatLng("55.75703".toDouble, "37.61614".toDouble)
    LatLngTool.distance(point1, point2, LengthUnit.KILOMETER)

  }

}
