package my.android.model

import akka.actor.Actor
import my.android.server.services._
import com.mongodb.{BasicDBObject, DBAddress, Mongo}
import akka.config.Supervision.Permanent
import scala.collection.JavaConversions._

import com.mongodb.casbah.MongoConnection
import com.mongodb.casbah.Imports._
import com.novus.salat.global._
import com.novus.salat._
import scala.Array
import my.android.server.services.UserGeoContext
import my.android.server.services.SilentPlace
import scala.Some
import my.android.server.resources.Cinema
import my.android.server.services.GeoPoint

case class GetClosestWithinCircle(point: GeoPoint, radius: Double)
case class ConstructUserGeoContext(point: GeoPoint)
case class AddUserByRegistration(user: User)
case class FindUserByCredentials(login: String, password: String)
case class GetAllMoscowCinemas()
case class AddPlace(point: GeoPoint, placeType: Long, descr: Option[String])
case class GetUserMongo(id: String, accountToken: String)
case class CreateUserMongo(id: String, accountToken: String)

class MongoActor extends Actor {
  self.lifeCycle = Permanent
  self.id = "mongo"

  val mongoConn = MongoConnection("127.0.0.1", 27017)

  var mongo: Mongo = _
  val amountOfRandomObjects: Int = 1000
  val mongoHost: String = "127.0.0.1"

  override def preStart() {

    mongo = new Mongo(new DBAddress(mongoHost, 27017, "geospacial"))
    getCollection.ensureIndex(new BasicDBObject("loc", "2d"), "geospacialIdx")
    getCollection.ensureIndex(new BasicDBObject("silence", "2d"), "geosilentIdx")
    getCollection.ensureIndex(new BasicDBObject("users", "2d"), "geousersIdx")
    getCollection.ensureIndex(new BasicDBObject("cinemas", "2d"), "geoCinemasIdx")
    mongoConn("geospacial")("locations").ensureIndex(new BasicDBObject("coordinates", "2d"), "geoCoordinatesIdx")
    mongoConn("geospacial")("cinemas").ensureIndex(new BasicDBObject("center", "2d"), "geoCenterCinemaIdx")
    addSilences()
    super.preStart()
  }

  protected def receive: Receive = {
    case GetClosestWithinCircle(point, radius) =>
      self reply withinCircleExample(point, radius)
    case GetAllMoscowCinemas() =>
      self reply allCinemasQuery()
    case ConstructUserGeoContext(point) =>
      self reply getUserGeoContext(point)
    case AddUserByRegistration(user) =>
      addUserQuery(user)
    case AddPlace(place, pt, descr) =>
      addPlaceQuery(place, pt, descr)
    case FindUserByCredentials(login, password) =>
      self reply  findUserByCredentialsQuery(login, password)
    case GetUserMongo(id, accountToken) =>
      self reply findUserBy(id, accountToken)
    case CreateUserMongo(id, accountToken) =>
      self reply createUserBy(id, accountToken)
  }

  def addUserQuery(user: User) {
    val newObj = MongoDBObject("fullname" -> user.fullname,
      "login" -> user.login,
      "password" -> user.password)
    mongo.getDB("geospacial").getCollection("users").insert(newObj)
  }

  def  addPlaceQuery(place: GeoPoint, placeType: Long, descr: Option[String]) {
    val builder = MongoDBObject.newBuilder
    builder += "coordinates" -> grater[GeoPoint].asDBObject(GeoPoint(place.lat, place.lng))
    builder += "geoPoint" -> Array(place.lat, place.lng)
    builder += "locationType" -> placeType
    descr.map( builder += "description" -> _)
    mongo.getDB("geospacial").getCollection("locations").insert(builder.result())
  }

  def findUserByCredentialsQuery(login: String, password: String): Option[User] = {
    val q = MongoDBObject("login" -> login, "password" -> password)
    val mongoColl = MongoConnection()("geospacial")("users")
    mongoColl.findOne(q) match {
      case Some(user) => Option(grater[User].asObject(user.asDBObject))
      case _ => None
    }
  }

  def findUserBy(id: String, accountToken: String): Option[User] = {
    val q = MongoDBObject("login" -> id, "accountToken" -> accountToken)
    val mongoColl = MongoConnection()("geospacial")("users")
    mongoColl.findOne(q) match {
      case Some(user) => Option(grater[User].asObject(user.asDBObject))
      case _ => None
    }
  }
  def createUserBy(id: String, accountToken: String): Option[User] = {
    val q = MongoDBObject("login" -> id, "accountToken" -> accountToken)
    val mongoColl = MongoConnection()("geospacial")("users")
    mongoColl.save(q)
    Option(User(id))
  }

  def withinCircleExample(point: GeoPoint, radius: Double) = {
    val circle = Array(Array(point.lat, point.lng), radius)
    val query = new BasicDBObject("loc", new BasicDBObject("$within", new BasicDBObject("$center", circle)))
    val res = getCollection.find(query).toArray.toSeq
    res
  }

  def allCinemasQuery() = mongoConn("geospacial")("cinemas").find().map(c => grater[Cinema].asObject(c.asDBObject)).toSeq

  def getUserGeoContext(point: GeoPoint) = {
    //val query = MongoDBObject("coordinates".$within $center (GeoCoords(50, 50), 10) , "locationType" -> 0L)
    def query(locationType: Long) = MongoDBObject(
      "coordinates" -> MongoDBObject(
        "$within" -> MongoDBObject(
          "$center" -> MongoDBList(
            MongoDBList(point.lat, point.lng),
            0.001))), "locationType" -> locationType )

      val silentRes = mongoConn("geospacial")("locations").find(query(0L)).toArray.toSeq
      val reminderRes = mongoConn("geospacial")("locations").find(query(1L)).toArray.toSeq

    def cinemasQuery = MongoDBObject(
      "center" -> MongoDBObject(
        "$within" -> MongoDBObject(
          "$center" -> MongoDBList(
            MongoDBList(point.lat, point.lng),
            0.03597963))))
      val cinemasRes = mongoConn("geospacial")("cinemas").find(cinemasQuery).toArray.toSeq

    val myCmd = new BasicDBObject()
    myCmd.append("geoNear", "locations")
    myCmd.append("near", Array(55.763901, 37.4151703))
    myCmd.append("distanceMultiplier",(6371 * Math.Pi / 180.0d))
    val ul = mongoConn("geospacial")("locations").underlying
    val db = ul.getDB
    val res = db.command(myCmd).get("results")        //TODO use distances in result

    val radianForOneKM = 1 / (6371 * Math.Pi / 180.0d)

    val queryNear = MongoDBObject(
      "coordinates" -> MongoDBObject(
        "$near" -> MongoDBList(55.763901, 37.4151703),
        "$maxDistance" -> 0.01))
    val nearRes = mongoConn("geospacial")("locations").find(queryNear).toArray.toSeq

    UserGeoContext(point,
      silentRes.map(c => grater[SilentPlace].asObject(c.asDBObject)).toSeq,
      reminderRes.map(c => grater[ReminderPlace].asObject(c.asDBObject)).toSeq,
      cinemasRes.map(c => grater[Cinema].asObject(c.asDBObject)).toSeq
    )
  }

  def addSilences() {
    val loc = new BasicDBObject("silencePoint", "MyRoom")
    loc.put("coordinates", Array(55.7639306,37.4152358))
    getCollection.update(new BasicDBObject("fullname", "Kitchen"), loc, true, false)
  }

  /*def addVenues() {
    val randomizer = new Random()

    for (i <- 1 to amountOfRandomObjects) {
      addVenue("Venue" + i,  Array(randomDeltaLat,randomDeltaLng))
      if(i == amountOfRandomObjects) println("Venue " + i + " added\n")
    }

    def randomDeltaLat = 55.80435 - randomizer.nextDouble() * (55.80435 - 55.80193)
    def randomDeltaLng = 37.396363 + randomizer.nextDouble() * (37.74771 - 37.736363)
  }

  def addVenue(pName: String, point: Array[Double])
  {
    val loc = new BasicDBObject("fullname", pName)
    loc.put("loc", point)
    getCollection.update(new BasicDBObject("fullname", pName), loc, true, false)
  }*/

  def getCollection =  {
    mongo.getDB("geospacial").getCollection("example")
  }
}
