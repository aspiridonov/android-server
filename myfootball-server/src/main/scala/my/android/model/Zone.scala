package my.android.model

import my.android.server.services.GeoPoint
import java.util.UUID


case class Zone(id: String = UUID.randomUUID().toString,
                name: String,
                center: GeoPoint,
                borders: Seq[GeoPoint],
                radius: Long) {

}
