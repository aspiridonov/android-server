package my.android.server.resources

import java.io.{File, FileInputStream}
import org.apache.poi.ss.usermodel._
import org.apache.poi.xssf.usermodel.XSSFWorkbook
import collection.mutable.ListBuffer
import scala.Option
import com.mongodb.casbah.MongoConnection
import com.mongodb.casbah.Imports._
import com.novus.salat.global._
import com.novus.salat._
import my.android.server.services.GeoPoint


object ExcelParser extends App{

  val mongoConn = MongoConnection("127.0.0.1", 27017)
  val mongoColl = mongoConn("geospacial")("cinemas")
  println("Started Excel parser:..........................................")
  val cinemaCollection = ListBuffer[Cinema]()
  //val in = classOf[ClassLoader].getResourceAsStream("/scala/my/android/server/resouces/moscow_cinemas.xlsx")
  val url = this.getClass.getClassLoader.getResource("moscow_cinemas.xlsx")

  val file = new File(url.getFile)
  val is = new FileInputStream(file)
  val wb = new XSSFWorkbook(is)

  val sheet = wb.getSheetAt(0)
  val it = sheet.iterator()
  while (it.hasNext) {

    val row = it.next()
    val cells = row.iterator()
    if (cells.hasNext) {
      val cinema = Cinema()
      while (cells.hasNext) {
        val cell = cells.next()
        cell.getCellType match {
          case Cell.CELL_TYPE_STRING => {
            val Coordinate = """.*\((.*)\)""".r
            cell.getStringCellValue match {
              case value if cell.getColumnIndex == 0 => cinema.name = value
              case Coordinate(latitude) if sheet.getRow(cell.getRowIndex).getCell(1).getStringCellValue == "ш" => {
                val Coordinate(longitude) = sheet.getRow(cell.getRowIndex + 1).getCell(cell.getColumnIndex).getStringCellValue
                cell.getColumnIndex match {
                  case 2 => cinema.lt = GeoPoint(latitude.toDouble, longitude.toDouble)
                  case 3 => cinema.rt = GeoPoint(latitude.toDouble, longitude.toDouble)
                  case 4 => cinema.lb = GeoPoint(latitude.toDouble, longitude.toDouble)
                  case 5 => cinema.rb = GeoPoint(latitude.toDouble, longitude.toDouble)
                  case 6 => cinema.center = GeoPoint(latitude.toDouble, longitude.toDouble)
                }
              }
              case Coordinate(value) if sheet.getRow(cell.getRowIndex).getCell(1).getStringCellValue == "д" => println("Ignore")
              case _ => println("unmatched: " + sheet.getRow(cell.getRowIndex).getCell(1))
            }
          }
          case Cell.CELL_TYPE_NUMERIC => System.out.print("[" + cell.getNumericCellValue + "]")
          case Cell.CELL_TYPE_FORMULA => System.out.print("[" + cell.getNumericCellValue + "]")
        }
      }
      cinemaCollection.append(cinema)
    }
  }

  //Write to DB
  val result = cinemaCollection.toSeq.filter(c => Option(c.name).isDefined && Option(c.lt).isDefined && Option(c.rt).isDefined && Option(c.lb).isDefined && Option(c.rb).isDefined && Option(c.center).isDefined)
  result.map(grater[Cinema].asDBObject(_)).map(mongoColl.insert(_))

  //Deserialize from DBObject to custom case class
  //val fromDB = mongoColl.find().map(c => grater[Cinema].asObject(c.asDBObject)).toSeq
  //fromDB
  println("Escel parsing finished...........")
}

case class Cinema(var name: String = "",
                  var lt: GeoPoint = null,
                  var rt: GeoPoint = null,
                  var lb: GeoPoint = null,
                  var rb: GeoPoint = null,
                  var center: GeoPoint = null)

