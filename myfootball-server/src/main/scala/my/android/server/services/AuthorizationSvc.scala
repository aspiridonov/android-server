package my.android.server.services

import AuthorizationSvc._
import cc.spray.Directives
import com.google.inject.Inject
import org.apache.commons.configuration.Configuration
import cc.spray.http.HttpHeaders.`Set-Cookie`
import my.android.server.net.{RichRequestContext, Authentication}
import util.DBUserPassAuthenticator
import java.lang.String
import akka.actor.Actor
import my.android.common.Container._
import my.android.common.Encryption
import my.android.model.{GetVKUser, GetUser, RegisterUser}
import cc.spray.http.HttpCookie
import scala.Some
import my.android.server.resources.ExcelParser
import com.codahale.jerkson.Json._
import cc.spray.http.HttpCookie
import my.android.model.GetUser
import scala.Some
import my.android.model.RegisterUser

@com.google.inject.Singleton
class AuthorizationSvc @Inject()(val security: Security,
                                 val config: Configuration) extends Directives with Authentication {
 lazy val model = Actor.registry.actorsFor("model").head
 val encryption = instance[Encryption]

  val service = {
    pathPrefix(MAIN_PATH) {
      authenticate(httpBasic("OnlyOneOutRealm", DBUserPassAuthenticator)) {
        user => {
          get {
            ctx => {
              new RichRequestContext(ctx).complete(
                user.username,
                `Set-Cookie`(HttpCookie(Security.AUTHORIZATION_COOKIE_NAME, "olKiphbVOrNo9v_jsoAoUwjXTIil-JzsgircLydVsrE"))
              )
            }
          }
        }
      }
    } ~
    pathPrefix(AUTHENTICATION) {
      path(AUTHENTICATION_VK) {
        get  {  //note: for post request use formFields in case of using form params while constructing instead of query params
          parameters('login.as[String], 'token.as[String]) {
            (login, token) => { ctx =>
              model ! GetUser(login, token, {
                case Some(user) => ctx.complete(generate(user))
              })
            }
          }
        }
      }
    } ~
    pathPrefix("registration") {
      path("email") {
        (get | post)   {  //note: for post request use formFields in case of using form params while constructing instead of query params
          parameters('email.as[String], 'fullName.as[String], 'password.as[String]) {
            (email, fullName, password) => { ctx =>
              model ! RegisterUser(fullName, email, password, {
                case Some(_) => ctx.complete("registered")
              })
            }
          }
      }
    }
    } ~
    pathPrefix("uploadFile") {
      get { ctx => {
            ExcelParser.main(Array(ctx.unmatchedPath))

            ctx.complete("Ok:" + ctx.unmatchedPath)
          }
      }
    }
  }

}

object AuthorizationSvc {
  private[AuthorizationSvc] val MAIN_PATH = "authorization"
  private[AuthorizationSvc] val EMAIL_REGISTRATION_PATH = "registration/email"
  private[AuthorizationSvc] val AUTHENTICATION = "authentication"
  private[AuthorizationSvc] val AUTHENTICATION_VK = "vk"
}