package my.android.server.services

import com.google.inject.Inject
import org.apache.commons.configuration.Configuration
import cc.spray.Directives
import my.android.server.net.Authentication
import GeoSvc._
import akka.actor.Actor
import my.android.model._
import com.codahale.jerkson.Json._
import com.typesafe.config.ConfigFactory
import my.android.model.AddPlace
import my.android.model.GetClosest
import my.android.model.GetAllCinemas
import my.android.model.GetGeoDistance
import my.android.model.GetUserGeoContext
import scala.Some
import my.android.server.resources.Cinema
import cc.spray.http.{HttpCharsets, MediaTypes, ContentType, HttpContent}

case class GeoPoint(lat: Double, lng: Double)
case class GeoLocation(var lat: Double = 0,
                       var lng: Double = 0,
                       var coordinates: com.mongodb.BasicDBList = null,
                       var locationType: Long = 0L,
                       var description:String = "",
                       var geoPoint: GeoPoint = GeoPoint(0,0))
case class SilentPlace(coordinates:GeoPoint = GeoPoint(0,0),
                        radius:Long = 0L,
                        areaCoordinates:Seq[GeoPoint] = Nil,
                        volumeLevel:Long = 0,
                        description:String = "")
case class ReminderPlace(coordinates:GeoPoint = GeoPoint(0,0),
                         description:String = "")
case class UserGeoContext(coordinates: GeoPoint = GeoPoint(0,0),
                           silentPlaces:Seq[SilentPlace] = Nil,
                           reminderPlaces:Seq[ReminderPlace] = Nil,
                           cinemas:Seq[Cinema] = Nil
                           )

@com.google.inject.Singleton
class GeoSvc @Inject()(val security: Security,
                       val config: Configuration) extends Directives with Authentication {

  lazy val actor = Actor.registry.actorFor[InMemoryModelActor].head

  lazy val mongoActor = Actor.registry.actorFor[MongoActor].head

  val service = {
    pathPrefix(MAIN_PATH) {
//      authenticate(httpBasic("OnlyOneOutRealm", DBUserPassAuthenticator)) {
//        user => {
          path("distance") {
            get {
              ctx => {
                actor ! GetGeoDistance({
                  case Some(distance) => ctx.complete("Distance:" + distance)
                })
              }
            }
          } ~
            path("coords") {
              get {
                parameters('lat.as[Double], 'lng.as[Double]) { (lat, lng) =>
                  ctx => {
                    actor ! GetClosest(GeoPoint(lat,lng),{
                      case points =>  ctx.complete(generate(points))
                    })

                  }
                }
              }
            }  ~
            path("helloworld") {
              get {
                ctx => {
                  val conf = ConfigFactory.load()
                  ctx.complete("Hello world " + conf.getString("simple-app.answer"))
                }
              }
            }  ~
            path("useractions") {
              get {
                parameters('lat.as[Double], 'lng.as[Double]) { (lat, lng) =>
                  ctx => {
                    actor ! GetUserGeoContext(GeoPoint(lat,lng),{
                      case actions => ctx.complete(HttpContent(ContentType(MediaTypes.`text/plain`, HttpCharsets.`UTF-8`),generate(actions)))
                    })

                  }
                }
              }
            } ~
            path("cinemas") {
              get {
                  ctx => {
                    actor ! GetAllCinemas({
                      case points =>  {
                        val p = points
                        ctx.complete(generate(points))
                      }
                    })
                }
              }
            } ~
            path("addPlace") {
              post {
                parameters('lat.as[Double], 'lng.as[Double], 'placeType.as[Int], 'descr ?) { (lat, lng, placeType, descr) => { ctx => {
                  mongoActor! AddPlace(GeoPoint(lat,lng), placeType, descr)
                  ctx.complete("ok")
                }
            }
          }
        }
      }
    }
  }

}
object GeoSvc {
  private[GeoSvc] val MAIN_PATH = "geo"
}

object PlaceType{
   val Silent = 0L
   val Reminder = 1L
}