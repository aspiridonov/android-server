package my.android.server.services.common

import cc.spray._
import cc.spray.utils.Logging
import cc.spray.http.{HttpRequest, HttpResponse, StatusCodes}
import http.HttpRequest


trait RestHttpServiceLogic extends HttpServiceLogic
{
  this: Logging =>

  def rejectionHandler = RejectionHandler.Default
}

case class RestHttpService(route: Route) extends HttpServiceActor with RestHttpServiceLogic {
  /*override protected def receive: PartialFunction[Any, Unit] = handleTimeouts orElse super.receive

  def handleTimeouts: Receive = {
    case Timeout(x: HttpRequest) =>
      self ! HttpResponse(StatusCodes.RequestTimeout, "Too late")
  }*/
}


