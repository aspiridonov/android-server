package my.android.server.services.util

import cc.spray._
import authentication.BasicUserContext
import akka.dispatch.AlreadyCompletedFuture
import akka.actor.Actor
import my.android.model.{User, FindUserByCredentials, MongoActor}


object DBUserPassAuthenticator extends UserPassAuthenticator[BasicUserContext] {
  lazy val mongoActor = Actor.registry.actorFor[MongoActor].head

  def apply(userPass: Option[(String, String)]) = new AlreadyCompletedFuture(
    Right {
      userPass.flatMap {
        case (login, pass) => {
            val result = mongoActor.ask(FindUserByCredentials(login, pass))
            result.get.asInstanceOf[Option[User]] match {
              case Some(user) => {
                val t = user
                Some(BasicUserContext(user.fullname))
              }
              case _ => None
            }
          }
        }
    }
  )
}
