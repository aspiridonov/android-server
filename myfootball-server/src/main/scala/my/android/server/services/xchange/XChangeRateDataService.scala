package my.android.server.services.xchange

import java.util.{ArrayList, Date}


class XChangeRateDataService {
  def getXRateData = {
    val list = new ArrayList[XChangeRatePair]();
    list.add(XChangeRatePair("EUR.USD", 1.3238, 1.3241, new Date()));
    list.add(XChangeRatePair("USD.JPY", 98.25, 98.27, new Date()));
    list.add(XChangeRatePair("EUR.JPY", 130.08, 130.13, new Date()));
    list.add(XChangeRatePair("GBP.USD", 1.4727, 1.4731, new Date()));
    list.add(XChangeRatePair("USD.CHF", 1.1396, 1.1400, new Date()));
    list.add(XChangeRatePair("EUR.CHF", 1.5091, 1.5094, new Date()));
    list.add(XChangeRatePair("EUR.GBP", 0.89844, 0.89890, new Date()));
    list
  }
}
