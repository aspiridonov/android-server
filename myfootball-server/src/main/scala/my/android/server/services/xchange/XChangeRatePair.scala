package my.android.server.services.xchange

import reflect.BeanProperty
import java.util.Date

case class XChangeRatePair(@BeanProperty symbol:String,
                       @BeanProperty bid: Double,
                       @BeanProperty offer: Double,
                       @BeanProperty lastUpdated: Date) {

}
