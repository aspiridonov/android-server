package my.android.server.services.xchange

import util.Random
import java.util.Properties
import javax.naming.{InitialContext, Context}


/*class XChangeRateSimulatedFeed {

  TopicSession pubSession;
  TopicPublisher publisher;
  TopicConnection connection;
  String _providerurl = "jnp://localhost:1099";
  String _ctxtFactory = "org.jnp.interfaces.NamingContextFactory";
  try {
    // Obtain JNDI Context
    val p = new Properties();
    p.put(Context.PROVIDER_URL, _providerurl);
    p.put(Context.INITIAL_CONTEXT_FACTORY, _ctxtFactory);
    p.put(Context.SECURITY_PRINCIPAL, "admin");
    p.put(Context.SECURITY_CREDENTIALS, "admin");
    val context = new InitialContext()(p);
    // Lookup a JMS connection factory
    val factory = context.lookup("ConnectionFactory").asInstanceOf[TopicConnectionFactory];
    // Create a JMS connection
    connection = factory.createTopicConnection();
    // Create publisher session
    pubSession = connection.createTopicSession(false, Session.AUTO_ACKNOWLEDGE);
    // Lookup a JMS topic (If you use JRun, topics are configured in  SERVER-INF\jrun-resources.xml)
    val topic = context.lookup("topic/XchangeRateUpdate").asInstanceOf[Topic];
    // Create a publisher and a subscriber
    publisher = pubSession.createPublisher(topic);
    val xRateInitialData = new XChangeRateDataService().getXRateData();
    random = new Random();
    while (true) {
      Thread.sleep(100);
      for (i <- xRateInitialData.size())
        {
          val message = pubSession.createObjectMessage();
          val xcrp = updatedXRate(xRateInitialData.get(i));
      message.setObject(xcrp);
      publisher.publish(message, Message.DEFAULT_DELIVERY_MODE, Message.DEFAULT_PRIORITY, 5 * 60 * 1000);
      System.out.println(message.toString());}
      }
      }
      catch (e: Exception)
      {
        case ex =>  ex.printStackTrace();
      }
}

object XChangeRateSimulatedFeed {
  val random: Random = _;

  private static XchangeRatePair updatedXRate(XchangeRatePair xchangeRatePair) {
    boolean bidOrOffer = random.nextBoolean();
    System.out.println("bidOrOffer " + bidOrOffer);
    boolean positiveOrNegative = random.nextBoolean();
    System.out.println("positiveOrNegative " + positiveOrNegative);
    double bid = xchangeRatePair.getBid();
    double offer = xchangeRatePair.getOffer();
    if(bidOrOffer == true){
      double bidChange = bid * 0.0009;
      if(positiveOrNegative == true){
        bid = bid + bidChange;
      } else
      {
        offer = offer - bidChange;
      }
      xchangeRatePair.setBid(bid);
    }else
    {
      double offerChange = offer * 0.0009;
      if(positiveOrNegative == true){
        offer = offer + offerChange;
      } else
      {
        offer = offer - offerChange;
      }
      xchangeRatePair.setOffer(offer);
    }
    xchangeRatePair.setLastUpdated(new Date());
    return xchangeRatePair;
  }
}
}*/
