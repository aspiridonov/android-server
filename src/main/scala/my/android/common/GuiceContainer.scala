package my.android.common

import com.google.inject.{Inject, Injector}

/**
 * Guice implementation of IoC container.
 */
@com.google.inject.Singleton
class GuiceContainer @Inject()(private val injector: Injector) extends Container {
  def get[T](clasz: Class[T]) = injector.getInstance(clasz)
}
