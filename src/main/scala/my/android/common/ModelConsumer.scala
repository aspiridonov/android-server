// Copyright 2011 Shopolate Inc. All rights reserved.
package my.android.common

import akka.actor.Actor

/**
 * Helpers for classes (mainly REST resources) that needs to access the read model of the API. This can be seen as an abstraction of a datasource.
 */
trait ModelConsumer {
  lazy val actor = Actor.registry.actorsFor("model").head

  def getModel[T](cmd: Any): T = (actor !!(cmd, 50000)).asInstanceOf[Option[T]].get
}