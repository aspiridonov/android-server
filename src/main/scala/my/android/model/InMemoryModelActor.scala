package my.android.model

import akka.actor.Actor
import akka.config.Supervision.Permanent
import collection.mutable.HashMap
import com.javadocmd.simplelatlng.{LatLngTool, LatLng}
import com.javadocmd.simplelatlng.util.LengthUnit
import com.mongodb.{BasicDBList, BasicDBObject, Mongo}

case class GetUser(id: String, callback: PartialFunction[Option[User], Unit])

case class GetUserReply(id: String)

case class GetGeoDistance(callback : PartialFunction[Option[Double], Unit])

class InMemoryModelActor extends Actor {
  self.lifeCycle = Permanent
  self.id = "model"

  private val usersPerId = new HashMap[String, User]


  override def preStart() {
    usersPerId.put("deil87", new User("deil87"))
    super.preStart()
  }

  protected def receive = {
    case GetUser(id, callback) =>
      callback(usersPerId.get(id))
    case GetUserReply(id) =>
      self reply usersPerId.get(id)
    case GetGeoDistance(callback) =>
      callback(Option(getDistance))
  }

  def getDistance: Double = {
    val mongo = new Mongo( "localhost" , 27017 )
    val db = mongo.getDB( "test" )
    val coll = db.getCollection("foo")
    val doc = new BasicDBObject()
    doc.put("routeId", "9327014.tcx")
    doc.put("id", "1105409")
    val loc = new BasicDBObject()
    loc.put("x", 5.1400218)
    loc.put("y", 52.0614276)
    doc.put("loc", loc)
    coll.insert(doc)

    val query = new BasicDBObject()
    val location = new BasicDBObject()
    val near = new BasicDBList()
    near.put( "0", 5.15 )
    near.put( "1", 52.069 )
    location.put("near", near)
    query.put("loc",loc)
    /* coll is a DBCollection */
    coll.find(query).limit(50)

    val point1 = new LatLng("55.75879".toDouble, "37.40633".toDouble)
    val point2 = new LatLng("55.75703".toDouble, "37.61614".toDouble)
    LatLngTool.distance(point1, point2, LengthUnit.KILOMETER)

  }

}
