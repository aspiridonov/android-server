package my.android.server

import com.google.inject.Guice
import com.google.inject.servlet.{ServletModule, GuiceServletContextListener}
import org.apache.commons.configuration.CompositeConfiguration
import org.apache.commons.configuration.SystemConfiguration
import org.apache.commons.configuration.XMLConfiguration
import org.apache.commons.configuration.ConfigurationUtils
import org.apache.commons.configuration.Configuration
import resources.History
import akka.config.Supervision
import akka.actor.{Actor, SupervisorFactory}
import cc.spray.RootService
import services.common.RestHttpService
import akka.config.Supervision.{OneForOneStrategy, Permanent, Supervise, SupervisorConfig}
import my.android.model.{InMemoryModelActor, ModelUserVerifier}
import my.android.common.{Container, GuiceContainer, Encryption}
import services.{GeoSvc, AuthorizationSvc, UserVerifier, PlayerSvc}


object ServerBoot{

   lazy val injector = Guice.createInjector(new ServletModule {
     override def configureServlets() {
       val config = new CompositeConfiguration
       config addConfiguration(new SystemConfiguration)
       config addConfiguration(new XMLConfiguration(ConfigurationUtils locate "common.xml"))

       bind(classOf[Container]) to classOf[GuiceContainer]
       bind(classOf[UserVerifier]) to classOf[ModelUserVerifier]
       bind(classOf[Configuration]) toInstance config
       bind(classOf[Encryption])

       bind(classOf[History]) // resource

       //Services
       bind(classOf[PlayerSvc])
       bind(classOf[AuthorizationSvc])
       bind(classOf[GeoSvc])

       //Сработает если в модуле есть хотя бы один ресурс (аннотация @Path)
       //например: bind(classOf[History])
       serve("*").`with`(classOf[com.sun.jersey.guice.spi.container.servlet.GuiceContainer])
     }
   })

   val player = injector.getInstance(classOf[PlayerSvc]).service
   val authorization = injector.getInstance(classOf[AuthorizationSvc]).service
   val geo = injector.getInstance(classOf[GeoSvc]).service

   val playerActor = Actor.actorOf(RestHttpService(player)).start()
   val authActor = Actor.actorOf(RestHttpService(authorization)).start()
   val geoActor = Actor.actorOf(RestHttpService(geo)).start()

   SupervisorFactory(
     SupervisorConfig(
       OneForOneStrategy(List(classOf[Exception])),
       Supervise(Actor.actorOf(new RootService(authActor, playerActor, geoActor)), Permanent) ::
       Supervise(Actor.actorOf(new InMemoryModelActor()).start(), Permanent) ::  Nil
     )
   ).newInstance

}

class ServerBoot extends GuiceServletContextListener {
  def getInjector = ServerBoot.injector
}
