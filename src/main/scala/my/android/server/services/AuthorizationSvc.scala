package my.android.server.services

import AuthorizationSvc._
import cc.spray.Directives
import com.google.inject.Inject
import org.apache.commons.configuration.Configuration
import cc.spray.http.HttpHeaders.`Set-Cookie`
import cc.spray.http.HttpCookie
import my.android.server.net.{RichRequestContext, Authentication}
import util.DBUserPassAuthenticator

@com.google.inject.Singleton
class AuthorizationSvc @Inject()(val security: Security,
                                 val config: Configuration) extends Directives with Authentication {

  val service = {
    pathPrefix(MAIN_PATH) {
      authenticate(httpBasic("MyFootballRealm", DBUserPassAuthenticator)) {
        user => {
          get {
            ctx => {
              new RichRequestContext(ctx).complete(
                user.username,
                `Set-Cookie`(HttpCookie(Security.AUTHORIZATION_COOKIE_NAME, "ubZdCYVPO-_lHz2924FjwY-4WnMicpln7lttNLMjPoU")))
            }
          }
        }
      }
    }
  }

}

object AuthorizationSvc {
  private[AuthorizationSvc] val MAIN_PATH = "authorization"
}