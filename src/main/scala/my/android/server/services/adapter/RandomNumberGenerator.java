package my.android.server.services.adapter;

import java.util.Random;

import flex.messaging.MessageBroker;
import flex.messaging.io.ArrayCollection;
import flex.messaging.messages.AsyncMessage;
import flex.messaging.messages.Message;
import flex.messaging.services.MessageService;
import flex.messaging.services.ServiceAdapter;
import flex.messaging.util.UUIDUtils;

/**
 * Custom Service Adapter which does the following
 * <p/>
 * 1) Generating an initial set of random numbers.
 * 2) Process the incoming messages.
 * 3) Responsible for pushing the message to all the clients.
 * 4) Responsible for sending the current state as part of ack.
 *
 * @author Siva Prasanna Kumar .P
 */
public class RandomNumberGenerator extends ServiceAdapter {
    /**
     * A thread which will be initialized and started on start of the
     * service adapter.
     */
    private static DataGenerator thread;
    /**
     * A collection of Random Generated numbers.
     */
    private static ArrayCollection randomNumbers = new ArrayCollection();

    public RandomNumberGenerator() {
        Random random = new Random();
        for (int i = 0; i < 6; i++) {
            randomNumbers.add(new Integer(random.nextInt(100)));
        }
    }

    public void start() {
        if (thread == null) {
            thread = new DataGenerator();
            thread.start();
        }
    }

    public void stop() {
        thread.running = false;
        thread = null;
    }

    /**
     * This is static Random Number generator class which acts as a source
     * or Data provider for the data push application.
     *
     * @author Siva Prasanna Kumar .P
     */
    public static class DataGenerator extends Thread {
        public boolean running = true;

        public void run() {
            MessageBroker msgBroker =
                    MessageBroker.getMessageBroker(null);
            String clientID = UUIDUtils.createUUID();
            Random random = new Random();
            while (running) {
                randomNumbers.clear();
                {
                    for (int i = 0; i < 6; i++) {
                        randomNumbers.add(new
                                Integer(random.nextInt(100)));
                    }
                }
            // creating a new async message and setting the
            // random generated number as content.
                AsyncMessage msg = new AsyncMessage();
            // setting the destination for this message.
                msg.setDestination("RandomDataPush");
                msg.setClientId(clientID);
                msg.setMessageId(UUIDUtils.createUUID());
                msg.setBody(randomNumbers);
                msgBroker.routeMessageToService(msg, null);
                try {
                    Thread.sleep(60000); // 1 minute delay.
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }
    }
    // @Override (uncomment this if you are using java 5 and above.)

    public Object invoke(Message message) {
        if (message.getBody().equals("New")) {
        // this send the current state to the client,
        // (random numbers previously generated as part of the
        // acknowledgment).
            return randomNumbers;
        } else {
            AsyncMessage newMessage = (AsyncMessage) message;
            newMessage.setBody(randomNumbers);
            MessageService msgService = (MessageService) getDestination().getService();
        // This is most important call which pushes,
        // the recivied msg to all the clients.
            msgService.pushMessageToClients(newMessage, false);
        }
        return null;
    }
}
