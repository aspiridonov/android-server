package my.android.server.services.util

import cc.spray._
import authentication.BasicUserContext
import akka.dispatch.AlreadyCompletedFuture



object DBUserPassAuthenticator extends UserPassAuthenticator[BasicUserContext] {
  def apply(userPass: Option[(String, String)]) = new AlreadyCompletedFuture(
    Right {
      userPass.flatMap {
        case (user, pass) => {
            if (user == "deil87" && pass == "mypass123") {
              Some(BasicUserContext(user))
            } else {
              None
            }
          }
        }
    }
  )
}
